(defproject com.galebach/clj-devicemap "2.64-SNAPSHOT"
  :description "clojure wrapper for Apache DeviceMap"
  :url "http://galebach.com"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.apache.devicemap/devicemap-client "1.1.0"
                  org.apache.devicemap/devicemap-data   "1.0.2-SNAPSHOT"]])
