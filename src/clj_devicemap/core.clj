(ns clj-devicemap.core
  (:import [org.apache.devicemap DeviceMapClient DeviceMapFactory]
           org.apache.devicemap.loader.LoaderOption))

(defn get-client []
  (DeviceMapFactory/getClient
   LoaderOption/URL "http://devicemap-vm.apache.org/data/latest"))

(def ^:dynamic *client* (get-client))

(defn classify-device [client user-agent-string]
  (.classifyDevice client user-agent-string))

(defn get-os [user-agent-string]
  (.getAttribute (classify-device *client* user-agent-string) "device_os"))

(defn get-model [user-agent-string]
  (.getAttribute (classify-device *client* user-agent-string) "model"))

(defn get-browser [user-agent-string]
  (.getAttribute (classify-device *client* user-agent-string) "mobile_browser"))

(defn get-browser-version [user-agent-string]
  (.getAttribute (classify-device *client* user-agent-string) "mobile_browser_version"))
