# clj-devicemap

Wraps Apache DeviceMap in Clojure, using the latest data source from
http://devicemap-vm.apache.org/data/latest

## Usage

in project.clj:
```clojure 
[com.galebach/clj-devicemap "0.92.99949-SNAPSHOT"]
```

```clojure 
(require '[clj-devicemap.core :refer [get-os classify-device]])
```

```clojure
(get-os "Mozilla/5.0 (Linux; U; Android 2.2; en; HTC Aria A6380 Build/ERE27) AppleWebKit/540.13+ (KHTML, like Gecko) Version/3.1 Mobile Safari/524.15.0")
;;"Android"
```

```clojure
get-browser
get-browser-version
get-model
```



## License

Copyright © 2015 T.S. Galebach

Eclipse License
